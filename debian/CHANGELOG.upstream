----------------------------------------------------------------------
Version 2.1
----------------------------------------------------------------------
* Fixed bug 196 "clustalx: user feedback about use of secondary structure 
  printed to console" - secondary structure is now used if specified
  in Alignment -> Alignment Parameters -> Secondary Structure Parameters
  UserParameters->getGui() should be used when ClustalW code needs to 
  know if a function has been called from ClustalX
  
* Fixed bug 204 "Nexus alignment format contain invalid line" - the amino
  acid alphabet line has been removed

* Missing/corrupted file names in ClustalX status messages have been
  fixed
  
* Fixed bug 175 "msf/pileup files cannot be read if sequences names are 
  all numbers" - this happened if a line such as
528244          .......... .......... .......... .......... .......... 
  was present in the first block of the file  
  
* Fixed bug 192 "Alignment in Phylip Format broken for big Alignments"

* Fixed bug 198 "Warning about divergent sequences gets printed to 
  console in ClustalX"
  
* Fixed bug 151 "clustalx doesn't switch to profile alignment mode when 
  profile12 is given on cmdline"

----------------------------------------------------------------------
Version 2.0.12
----------------------------------------------------------------------

* Fixed bug 189 "Fixed filename used for iteration":
Now Creating temporary file and added error check

* Fixed bug 180 "Pairwise NJ tree: final bracket missing"

* Fixed bug 178 "Seg-fault on 64bit when -quicktree -upgma for sequences with high identity":
  Using relative error now to avoid unsafe comparison which led to
  incorrect branching

* Fixed Bug 176 "iteration doesn't iterate if -usetree is used as well"

* Fixed bug 162 "percent identity file option in clustalW not working":
Added -pim as command line option. See help

* Fixed bug 155 "upgma trees cannot be read"

* Fixed bug 147 "report duplicate sequences":
 "ClustalW/X now report offending sequences which are empty, duplicates etc

* Fixed bug 134 "Exit when encountering unrecognized cmdline params":
ClustalW now exits when encountering invalid values for command line
arguments instead of just reverting to default values

* Fixed bug 185 "clustal alignments differ between interactive and commandline mode"
window-gap and ktuple initialisation now fixed and made the same
between commandline and interactive mode

* Fixed bug 184 "error messages are send to stdout"

* Fixed bug when weights are 0, and nseq > INT_SCALE_FACTOR in UPGMA
  code (see RootedGuideTree.cpp)

* General code cleanup
- Introduced return values where control reached end of non-void function
- Removed unused variables 
- Removed comparison between signed and unsigned integer expressions
- Removed deprecated conversion from string constant to char*



----------------------------------------------------------------------
Version 2.0.11
----------------------------------------------------------------------

* fixed file extension bug 166 in interactive mode

* Fixed bug 169, memory violation for DNA/RNA k-tuple

* Cut down distance calculation, symmetric matrix


----------------------------------------------------------------------
Version 2.0.10
----------------------------------------------------------------------

* Fixed g++-4.3 compilation errors

* Added new -quiet command line flag

* Added new -stats=<file> command line flag

* Fixed bug 142: command separator can now be mixed "/" and "-" on all platforms

* Fixed bug 141: profile merging and saving failed

* Fixed bug 139: saving of column quality scores

* Updated help files (new flags, new colour parameter format)


----------------------------------------------------------------------
Version 2.0.9
----------------------------------------------------------------------

* GUI now responding and giving feedback during alignment

* automatic automake os detection

* new OS_ define (set by clustalw/configure clustalx/clustalx.pro)

* got rid of qt3 dependencies

* removed QScrollArea bug workaround (fixed in Qt 4.3)

* Fixed bug 135: Last sequence and last residue not displayed on MacOSX

* Fixed bug 123: secondary structure profile alignment in clustalX on Mac

* Fixed g++-4.3 build error (include climits.h)


----------------------------------------------------------------------
Version 2.0.8
----------------------------------------------------------------------

* Implemented maxseqlen cmdline switch

* Updated help-file

* Fixed Bug 123: loading profile using gap penalties (ClustalX, Mac)

* Fixed bug 133: providing profiles on command line fails (ClustalX)

* Fixed bug 125: Angle bracket inside sequence

* Fixed bug 129: Early exit on empty sequence

* Fixed a couple of possible memory leaks


----------------------------------------------------------------------
Version 2.0.7
----------------------------------------------------------------------

* Fixed bug 121: CRLF  in sequence names (Pearson) are not trimmed

* Fixed bug 122: convert option broken

* Fixed reopened bug 114: profile alignment input didn't work with new
  getSeqRange routines

* Fixed bug 119: build with g++ 4.3

----------------------------------------------------------------------
Version 2.0.6
----------------------------------------------------------------------

* Fixed bug 77: fasta input performance issue

* Fixed bug 114: segfault while doing profile alignment with secondary
  structure mask

* Removed unncessary id console output in EMBLFileParser.cpp

* Fixed Bugs 108 and 109 and allowed mixed-case command line options


----------------------------------------------------------------------
Version 2.0.5
----------------------------------------------------------------------

* Fixed bug 105: Disallowed combination of "Reset Gaps"
  and Iteration in GUI

* Fixed bug 104 "reset all gaps doesn't work"

* Changed command line separator for Mac to hyphen instead slash

* Fixed full command line parsing for ClustalX after help flag


----------------------------------------------------------------------
Version 2.0.4 
----------------------------------------------------------------------

* Updated URLs in help files

* Fixed bug 96: error message when loading alignment with identical
  sequence names

* Made console output look more like 1.83

* Fixed bug 100: "Scores in clustalw2.0 don't match clustalw1.83"
  getMatrix was called with wrong scaling factor

* Fixed bug 99: "stars in input not ignored"
  Asterisks were changed to gaps in alignment

* New command line option: -fullhelp which dumps the built-in help
  file content.

* Quickfix for bug 94 "quicktree seqfault"


----------------------------------------------------------------------
<= Version 2.0.3 
----------------------------------------------------------------------

* Added LICENSE file to distribution
This file contains the information about commercial licensing of
clustal as well as FAQ for licensing issues

* Added README file to distribution
This is the file that lists the files and directories on the Clustal
FTP site.  It also includes acknowledgements of people who have
contributed binaries

* Removed .pro Qt file from the distribution
pro-file should be generated anew using qmake and modified according
to build requirements, i.e. no need for version control.

* Fixed bug where ClustalX2 was not processing command line args

* Fixed Segfault on opening helpfile.  Happened on Linux only with -O2
and when calling binary directly, not using the wrapper

* Added debian packaging files

* Added support for help command line flag GUI/xmenus version
When requesting help file, graphical version of command line help is
displayed (1.83 tried to open clustalw help)

* Added complete automake (configure etc) system according to the
following websites:
- http://www.openismus.com/documents/linux/automake/automake.shtml
- http://www.bioinf.uni-freiburg.de/~mmann/HowTo/automake.html

* clustalw files source files have been moved to subdir

* Fixed bug #53 change MAXNAMESTODISPLAY back to 10 from 30.
This fixes problem of large amount of space between sequence name and
actual alignment in clustal output files

* This solves bug #72 with long lines (5000+) in fasta files
changed code to use strings rather than arrays.  Needed to add delim
parameter to getline in order to read files formatted for different
OSs on different platforms.

* Fixed Bug 79:
"The count of amino acids in the ClustalX Postscript output not correct"
Off-by-one issue

* ClustalX and ClustalW version numbers are now the same and defined in
ClustalW code (automake)

* Fixed problem with compilation of ClustalX2 with gcc3
avoiding gcc3 error message: cannot declare member function
QDir::currentPath'

* Target now clustalw2 instead of clustalw

* Fixed Bug 46
added in aminoacid code O for pyrrolysine

* Fixed bug 89
changed clustalw2.0 to conform to 1.83 behaviour for width of sequence
ID and alignment

* Fixed bug 90
changed clustalw2.0 to conform to 1.83 behaviour leading spaces are
stripped from FASTA identifiers.

* Fixed bug 91
Clustalw2.0 now handles pseudo-FASTA/MoST format files. Strips out
numbers and spaces.

